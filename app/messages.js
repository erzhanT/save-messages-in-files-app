const express = require('express');
const router = express.Router();
const fs = require('fs')
const path = "./messages";


router.get('/', (req, res) => {

    fs.readdir('./messages', (err, files) => {
        let data = [];
        files.forEach(file => {
            data.push(JSON.parse(fs.readFileSync(path + '/' + file)));
        });
        const takeFiveMessages = data.slice(-5);
        res.send(takeFiveMessages);
    });

});

router.post('/', (req, res) => {
    const message = req.body;
    message.date = new Date().toISOString();
    fs.writeFileSync(`./messages/${message.date}.txt`, JSON.stringify(message));
    res.send({message: 'File saved'});
});

module.exports = router;